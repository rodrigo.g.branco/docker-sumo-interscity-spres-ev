"""
Process sparse json files into a single csv

Usage: python summary-jsonpath2.py <input_dir> <output_file_csv>
"""

import json
import os
import sys
import pandas as pd
from jsonpath_ng import parse
import numpy as np

if __name__ == "__main__":
    input_dir = sys.argv[1]

    if len(sys.argv) != 3:
        sys.exit("Usage: summary-jsonpath2.py <input_dir> <output_file_csv>")

    csvdata = {
        "scenario": [],
        "instance": [],
        "ev": [],
        "alg": [],
        "seed": [],
        "tl": [],
        "rt": [],
        "ttt": [],
        "eff": [],
        "imp": [],
        "perc": [],
        "teleported": [],
        "preemptime": [],
        "n_teleported": [],
        "avg_trip_speed": [],
        "avg_trip_timeloss": [],
        "avg_trip_speed_perc": [],
        "avg_trip_timeloss_perc": [],
        "teleported_perc": [],
    }

    # scenarios = ["sp", "ny"]
    # instance = [1, 2, 3, 4, 5]

    # scenarios = ["oneintersection", "turinkap", "colognekap"]
    scenarios = ["augmented-downtown"]
    # scenarios = ["sumohighteleporttime"]
    # scenarios = ["cologne"]
    instance = [2]

    for scenario in scenarios:
        for i in instance:
            base_path = f"{input_dir}/{scenario}/{scenario}-{i}/results/staticdynamic"

            for r in os.listdir(base_path):
                if r.split(".")[-1] != "json":
                    continue

                params = r.split("!")
                ev = params[1].split("_")[0]
                seed = params[2].split("_")[0]
                alg = params[3].split(".json")[0].split("_")[0]

                print(f"processing {base_path}/{r}...")

                tmp = open(f"{base_path}/{r}", "r", encoding="utf-8")
                data = json.loads(tmp.read())
                tmp.close()
                evs_teleporteds = parse("$.evs_teleported").find(data)[0].value
                vehs_data = parse("$.vehs").find(data)[0].value

                teleported = ev in evs_teleporteds

                tls = parse("$.tls").find(data)[0].value

                preemption_time = []
                for tl in tls:
                    total = 0
                    for actindex in range(1, len(tls[tl]), 2):
                        if tls[tl][actindex - 1] != -1 and tls[tl][actindex] != -1:
                            total += tls[tl][actindex] - tls[tl][actindex - 1]
                    preemption_time.append(total)

                csvdata["scenario"].append(scenario)
                csvdata["instance"].append(i)
                csvdata["ev"].append(ev)
                csvdata["alg"].append(alg)
                csvdata["seed"].append(seed)
                csvdata["teleported"].append(teleported)
                csvdata["rt"].append(parse("$.param[2]").find(data)[0].value)
                csvdata["imp"].append(np.NaN)
                csvdata["perc"].append(np.NaN)
                csvdata["avg_trip_speed"].append(
                    parse("$.avg_trip_speed").find(data)[0].value
                )
                csvdata["avg_trip_timeloss"].append(
                    parse("$.avg_trip_timeloss").find(data)[0].value
                )
                csvdata["avg_trip_speed_perc"].append(np.NaN)
                csvdata["avg_trip_timeloss_perc"].append(np.NaN)
                csvdata["teleported_perc"].append(np.NaN)
                csvdata["preemptime"].append(
                    np.mean(preemption_time) if len(preemption_time) > 0 else np.NaN
                )
                csvdata["n_teleported"].append(
                    parse("$.n_teleported").find(data)[0].value
                )
                if not teleported:
                    csvdata["tl"].append(vehs_data[ev][2])
                    csvdata["ttt"].append(vehs_data[ev][1])
                    csvdata["eff"].append(
                        float(vehs_data[ev][1]) - float(vehs_data[ev][2])
                    )
                else:
                    csvdata["tl"].append(np.NaN)
                    csvdata["ttt"].append(np.NaN)
                    csvdata["eff"].append(np.NaN)

    df = pd.DataFrame(csvdata)

    df_other = df[(df["alg"] != "no-preemption") & (~df["teleported"])]

    tl_imp = []

    for index, row in df_other.iterrows():
        df_nopreempt = df[
            (df["alg"] == "no-preemption")
            & (df["scenario"] == row.scenario)
            & (df["ev"] == row.ev)
            & (df["seed"] == row.seed)
            & (~df["teleported"])
        ]

        if not df_nopreempt.empty:
            tl_before = float(df_nopreempt["tl"].iloc[0])
            tl_after = float(row.tl)
            # the higher, the better
            df.loc[index, "imp"] = (
                tl_before / tl_after
                if tl_after < tl_before
                else -1 * (tl_after / tl_before)
            )
            # the higher, the better
            df.loc[index, "perc"] = (
                (1 - tl_after / tl_before)
                if tl_after < tl_before
                else ((tl_after / tl_before) - 1) * -1
            ) * 100

            avg_trip_speed_before = float(df_nopreempt["avg_trip_speed"].iloc[0])
            avg_trip_speed_after = float(row.avg_trip_speed)
            # the higher, the better
            df.loc[index, "avg_trip_speed_perc"] = (
                avg_trip_speed_after / avg_trip_speed_before - 1
            ) * 100

            avg_trip_timeloss_before = float(df_nopreempt["avg_trip_timeloss"].iloc[0])
            avg_trip_timeloss_after = float(row.avg_trip_timeloss)

            # the lower, the better
            df.loc[index, "avg_trip_timeloss_perc"] = (
                (1 - avg_trip_timeloss_after / avg_trip_timeloss_before)
                if avg_trip_timeloss_after < avg_trip_timeloss_before
                else ((avg_trip_timeloss_after / avg_trip_timeloss_before) - 1) * -1
            ) * 100

            n_teleported_before = float(df_nopreempt["n_teleported"].iloc[0])
            n_teleported_after = float(row.n_teleported)

            # the lower, the better
            if n_teleported_before > 0:
                df.loc[index, "teleported_perc"] = (
                    (1 - n_teleported_after / n_teleported_before)
                    if n_teleported_after < n_teleported_before
                    else ((n_teleported_after / n_teleported_before) - 1) * -1
                ) * 100

    df.to_csv(sys.argv[2], index=False)
