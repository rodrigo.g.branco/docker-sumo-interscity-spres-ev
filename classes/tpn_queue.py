import math
import random
from classes.petri_util import PetriUtil
from classes.preemption_strategy import PreemptionStrategy


class TPNQueueStrategy(PreemptionStrategy):
    # constants: Marshall, P.S. and W.D. Berg, Design Guidelines for Railroad Preemption at
    # Signalized Intersections. Institute of Transportation Engineers Journal, 1997.
    def configure(self):
        self.infinity = True
        self.k = 0.149129457  # in veh/m = 240 vehicles per mile
        self.s = 0.444444444  # in veh/s = 1600 vehicles per hour
        self.a = 2.6  # https://sumo.dlr.de/docs/Definition_of_Vehicles,_Vehicle_Types,_and_Routes.html
        self.pn = None
        self.edge_of_tl = {}
        self.tl_of_edges = []
        self.edges_of_tl = []
        self.index = None
        self.executed_states = []
        self.has_tl = []
        self.ev_vmax = None
        self.min_gap = 0
        self.veh_length = 0

        # cancel state
        self.time_stopped = 0
        self.cancel_state = 0

        self.cancel_threshold = 5

        self.time_moving = 0
        self.moving_threshold = self.cancel_threshold

        self.time_cooldown = 0
        self.cooldown_threshold = 0

        self.reaction_time_red_light = 10

        self.lane_occupancy_threshold = 0.6

    def execute_step(self, step, ev_entered_in_simulation, in_simulation):
        super().execute_step(step, ev_entered_in_simulation, in_simulation)

        if self.ev_entered and not self.ev_exited:
            self.index = self.mw.get_route_index_of_vehicle(self.ev)
            if self.ev_vmax == None:
                # to keep experiments reproducible
                self.ev_vmax = self.mw.get_vehicle_max_speed(self.ev)
                self.min_gap = self.mw.get_min_gap(self.ev)
                self.veh_length = self.mw.get_vehicle_length(self.ev)

                self.tl_of_edges = [None] * len(self.conf.edges_order)
                self.edges_of_tl = [None] * len(self.conf.edges_order)
                self.has_tl = [False] * len(self.conf.edges_order)

                for i, e in reversed(list(enumerate(self.conf.edges_order))):
                    if "tl" in self.conf.edges[e]:
                        self.tl_of_edges[i] = self.conf.edges[e]["tl"]["name"]
                        self.edges_of_tl[i] = e
                        self.has_tl[i] = True
                    elif i < len(self.conf.edges_order) - 1:
                        self.tl_of_edges[i] = self.tl_of_edges[i + 1]
                        self.edges_of_tl[i] = self.edges_of_tl[i + 1]

                for edge_id in self.conf.edges_with_tl:
                    self.edge_of_tl[self.conf.edges[edge_id]["tl"]["name"]] = edge_id

            if self.pn is None:
                self.build_petri_net(step)

            self.fire_transitions(step)

            self.check_crossing(step)

            self.update_tpn_times(step)

            ev_speed = self.mw.get_vehicle_speed(self.ev)
            ev_acc = self.mw.get_vehicle_acc(self.ev)

            self.change_cancel_state(step, ev_speed, ev_acc)

        elif self.ev_entered and self.ev_exited:
            self.global_cancel(step)

    def global_cancel(self, step):
        if "p7" not in self.pn.get_marking():
            t = self.pn.transition("t5")
            if len(t.modes()) > 0 and t.enabled(t.modes()[0]):
                t.fire(t.modes()[0])
        self.fire_transitions(step)
        self.check_crossing(step)

    def check_crossing(self, step):
        # check crossed TL
        if (
            self.index > 0
            and self.tl_of_edges[self.index - 1] != self.tl_of_edges[self.index]
        ):
            trans_name = "{}_{}".format("t3", self.tl_of_edges[self.index - 1])
            if self.pn.has_transition(trans_name):
                t = self.pn.transition(trans_name)
                if len(t.modes()) > 0 and t.enabled(t.modes()[0]):
                    t.fire(t.modes()[0])
                    print("firing {}...".format(t.name))

    def update_tpn_times(self, step):
        if self.pn is not None:
            time_to_open_tls = self.get_min_times(step)

            for tl in time_to_open_tls:
                t0 = self.pn.transition("{}_{}".format("t0", tl))
                t0.update_min_time(time_to_open_tls[tl])

    def fire_transitions(self, step):
        if self.pn is not None:
            self.pn.time(step)

            while True:

                enabled_trans = [
                    t
                    for t in self.pn.transition()
                    if "t3" not in t.name
                    and "t5" not in t.name
                    and len(t.modes()) > 0
                    and t.enabled(t.modes()[0])
                ]

                if len(enabled_trans) == 0:
                    break

                for t in enabled_trans:
                    if len(t.modes()) == 0:
                        continue

                    print("firing {}...".format(t.name))
                    t.fire(t.modes()[0])

                    for m in self.pn.get_marking():
                        if "p1_" in m and m not in self.executed_states:
                            self.executed_states.append(m)
                            tl_name = "_".join(m.split("_")[1:])
                            edge_id = self.edge_of_tl[tl_name]
                            self.open_tl_at_time_by_cycles(1, tl_name, step)

                            """for tl_adj in self.conf.edges[edge_id]["tl"]["adj"]:
                                if tl_adj not in self.conf.tls:
                                    tl_adj_conf = self.conf.edges[edge_id]["tl"]["adj"][
                                        tl_adj
                                    ]
                                    self.open_adj_tls(
                                        tl_adj,
                                        tl_adj_conf["phase"],
                                        tl_adj_conf["numberOfPhases"],
                                    ) """

                        if "p4_" in m and m not in self.executed_states:
                            self.executed_states.append(m)

                            tl_to_restore = "_".join(m.split("_")[1:])
                            edge_id = self.edge_of_tl[tl_to_restore]

                            if (
                                tl_to_restore in self.orch.active_evs_by_tl
                                and self.ev in self.orch.active_evs_by_tl[tl_to_restore]
                                and len(self.orch.active_evs_by_tl[tl_to_restore]) > 1
                            ):
                                self.orch.active_evs_by_tl[tl_to_restore].remove(
                                    self.ev
                                )
                            else:
                                self.orch.schedule_sync(
                                    self.ev, tl_to_restore, self.conf, self.infinity
                                )

                            """for tl_adj in self.conf.edges[edge_id]["tl"]["adj"]:
                                if tl_adj not in self.conf.tls:
                                    self.clear_adj_tls(tl_adj) """

    def build_petri_net(self, step):
        self.executed_states = []
        time_to_open_tls = self.get_min_times(step)

        self.pn = PetriUtil().build_petri_net(
            self.conf, self.conf.edges_order, self.index, time_to_open_tls
        )

    def get_min_times(self, step):
        time_to_open_tls = {}

        queue_length = 0

        for i in range(self.index, len(self.conf.edges_order)):
            edge = self.conf.edges_order[i]
            
            queue_length += self.get_queue_length(edge)
            
            #tl = self.tl_of_edges[i]

            #if self.has_tl[i] and i != self.index and self.mw.get_lane_occupancy("{}_0".format(edge)) <= 0.05:
            #    time_to_open_tls[tl] = step + 1
            #elif self.has_tl[i]:
            if self.has_tl[i]:
                intersection_distance = self.get_distance_to_tl(tl, edge)

                max_v = min(
                    self.ev_vmax, self.mw.get_max_speed_of_lane("{}_0".format(edge))
                )

                arrival_time = intersection_distance / max_v

                qflush = 0

                if queue_length > 0:
                    qflush = (queue_length * self.k) / self.s

                    self.dlast = math.pow(max_v, 2) / (2 * self.a)

                    if queue_length <= self.dlast:
                        qflush += math.sqrt((2 * queue_length) / self.a)
                    else:
                        qflush += max_v / self.a - (queue_length - self.dlast) / max_v

                tflush = (
                    self.conf.edges[edge]["tl"]["y"]["duration"]
                    + self.conf.edges[edge]["tl"]["r"]["duration"]
                )

                time_to_open_tls[tl] = step + math.floor(
                    max(
                        arrival_time - qflush - tflush - self.reaction_time_red_light, 0
                    )
                )

        return time_to_open_tls

    def get_distance_to_tl(self, tl_current, tl_edge):
        next_tls = self.mw.get_next_tls(self.ev)
        if len(next_tls) > 0 and tl_current in next_tls[0][0]:
            return next_tls[0][2]

        distance = self.mw.get_distance_edge_vehicle(self.ev, tl_edge)

        if distance < 0 and tl_current in self.mw.get_junctions():
            pos_tl_x, pos_tl_y = self.mw.get_tl_position(tl_current)
            distance = self.mw.get_2d_distance_vehicle(self.ev, pos_tl_x, pos_tl_y)

        return distance

    def get_queue_length(self, edge):
        # max_hn = 0
        # for j in range(0, self.mw.get_num_lanes(edge)):
        #    lane_id = "{}_{}".format(edge, j)
        #    max_hn = max(self.mw.get_halting_number(lane_id), max_hn)

        return self.mw.get_halting_number("{}_0".format(edge)) * (
            self.veh_length + self.min_gap
        )

    def change_cancel_state(self, step, ev_speed, ev_acc):
        # From any state, if there are no more tls to pass, go to the final state
        if self.tl_of_edges[self.index] == None:
            self.cancel_state = 7

        # Normal operation state
        if self.cancel_state == 0:
            if ev_speed <= 0.1 and ev_acc <= 0:
                # EV is stopped
                self.cancel_state = 1
                self.time_stopped += 1
            else:
                # reset timer
                self.time_stopped = 0
        # Ev is stopping
        elif self.cancel_state == 1:
            if not (ev_speed <= 0.1 and ev_acc <= 0):
                # EV is moving
                self.cancel_state = 0
                self.time_stopped = 0
            elif (
                self.tl_of_edges[self.index] != None
                and self.get_distance_to_tl(
                    self.tl_of_edges[self.index], self.conf.edges_order[self.index]
                )
                <= self.veh_length
            ):
                # EV is stopped but it is the first in the line. Keep preemption
                self.cancel_state = 0
                self.time_stopped = 0
            elif self.time_stopped <= self.cancel_threshold:
                # increase timer
                self.time_stopped += 1
            else:
                # change state
                self.cancel_state = 2
        # Cancelling
        elif self.cancel_state == 2:
            print("canceling...")
            self.global_cancel(step)
            self.time_stopped = 0
            if self.tl_of_edges[self.index] != None:
                tl_info = self.conf.edges[self.edges_of_tl[self.index]]["tl"]
                self.stop_threshold = (
                    self.cancel_threshold
                    + tl_info["g"]["duration"]
                    + tl_info["y"]["duration"]
                    + tl_info["r"]["duration"]
                )
            self.cancel_state = 3
        # Cancel state, check if EV moves or stand still
        elif self.cancel_state == 3:
            if not (ev_speed <= 0.1 and ev_acc <= 0):
                # EV is moving
                self.cancel_state = 4
                self.time_stopped = 0
                self.time_moving = 0
            elif self.time_stopped <= self.stop_threshold:
                # increase timer
                self.time_stopped += 1
            else:
                # change state
                self.cancel_state = 5
                self.time_moving = 0
        # EV moved, check if we can keep that
        elif self.cancel_state == 4:
            if not (ev_speed <= 0.1 and ev_acc <= 0):
                self.time_moving += 1
                if self.time_moving > self.moving_threshold:
                    self.cancel_state = 5
                    self.time_moving = 0
            else:
                self.cancel_state = 3
                self.time_stopped = 0
        # Rebuild the TPN
        elif self.cancel_state == 5:
            print("rebuilding TPN...")
            self.build_petri_net(step)
            self.fire_transitions(step)
            self.time_cooldown = 0
            queue_length = self.get_queue_length(self.conf.edges_order[self.index])
            dtl = (
                0
                if self.tl_of_edges[self.index] == None
                else self.get_distance_to_tl(
                    self.tl_of_edges[self.index], self.conf.edges_order[self.index]
                )
            )
            self.cooldown_threshold = self.stop_threshold = math.ceil(
                ((min(queue_length, dtl) * self.k) / self.s) + self.moving_threshold
            )
            self.cancel_state = 6
        # Cooldown state
        elif self.cancel_state == 6:
            if self.time_cooldown <= self.cooldown_threshold:
                self.time_cooldown += 1
            else:
                self.cancel_state = 0
                self.time_stopped = 0
        # Nothing to do... just cancel (sink state)
        elif self.cancel_state == 7:
            print("final canceling...")
            self.global_cancel(step)

